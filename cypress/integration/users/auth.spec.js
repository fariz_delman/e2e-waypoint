const usernameInput = "[data-testid='username-input']"
const passwordInput = "[data-testid='password-input']"
const loginSubmit = "[data-testid='login-button']"

describe('user stories', () => {
  it('as a user I want to login using my credentials so I can use the service', () => {
    cy.visit('/')
    cy.url().should('include', '/login')

    cy.get(usernameInput).type('admin')
    cy.get(passwordInput).type('password')
    cy.get(loginSubmit).click()

    cy.url().should('include', '/')
  })

  it('when I use wrong credentials, I need to know which one is incorrect so I can determine all of my combination', () => {
    cy.visit('/')
    cy.url().should('include', '/login')

    cy.get(usernameInput).type('admin')
    cy.get(passwordInput).type('admin')
    cy.get(loginSubmit).click()

    cy.contains(/wrong password/i)
    cy.get(passwordInput).type('password')

    cy.url().should('include', '/')
  })

  it('When I forgot my password, I want to reset my current password so I can use my new password to login', () => {
    cy.visit('/')
    cy.url().should('include', '/login')

    cy.get(usernameInput).type('admin')
    cy.get(passwordInput).type('admin')
    cy.get(loginSubmit).click()

    cy.contains(/wrong password/i)

    cy.get('a').contains('Forgot Password?').click()

    cy.url().should('include', '/forgot-password')
    cy.contains(/forget password/i)
  })
})
