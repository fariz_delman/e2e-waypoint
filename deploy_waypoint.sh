#!/usr/bin/bash

set -eux

WORKSPACE=$1

echo "> deploying redis..."

waypoint build -app=redis -workspace=$WORKSPACE
waypoint deploy -app=redis -workspace=$WORKSPACE -release=false > redis.log 2>&1

echo "> deploying postgres..."

waypoint build -app=postgres -workspace=$WORKSPACE
waypoint deploy -app=postgres -workspace=$WORKSPACE -release=false > postgres.log 2>&1

# now this is a dirty hack since postgres & redis can't work well behind
# reverse proxy, we need to inject their container-name so the application
# will connect to them based on their container-name.
#
# and there is no proper api from waypoint 

# first of all, we need to get the container name.
REDIS_CONTAINER_NAME=$(cat redis.log | grep redis- | awk '{print $6}')
POSTGRES_CONTAINER_NAME=$(cat postgres.log | grep postgres- | awk '{print $6}')

# and then let's regenerate our local waypoint.hcl
sed -i "s/REDIS_CONTAINER_NAME/$REDIS_CONTAINER_NAME/g" waypoint.hcl
sed -i "s/POSTGRES_CONTAINER_NAME/$POSTGRES_CONTAINER_NAME/g" waypoint.hcl

# ok, now let's deploy the backend and get the endpoint for it

echo "> deploying backend..."

waypoint build -app=backend-worker -workspace=$WORKSPACE
waypoint deploy -app=backend-worker -workspace=$WORKSPACE -release=false

waypoint build -app=backend -workspace=$WORKSPACE
waypoint deploy -app=backend -workspace=$WORKSPACE > backend.log 2>&1

BACKEND_ENDPOINT=$(cat backend.log | grep -i "deployment url" | grep waypoint.run | awk '{print $3}')

# now deploy the frontend

echo "> deploying forntend..."

waypoint build -app=frontend -workspace=$WORKSPACE
waypoint deploy -app=frontend -workspace=$WORKSPACE > frontend.log 2>&1

FRONTEND_ENDPOINT=$(cat frontend.log | grep -i "deployment url" | grep waypoint.run | awk '{print $3}')

# let's update data to redis for api-gw
# FIXME: for this demo i use lambda.store
redis-cli -u redis://9f6313d92d6e48bf8fc00f414de13f90@us1-sweet-orca-31794.lambda.store:31794 set $FRONTEND_ENDPOINT $BACKEND_ENDPOINT

echo "> done i guess?"
