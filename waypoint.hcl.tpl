project = \"$WAYPOINT_PROJECT\"

app \"redis\" {
  build {
    use \"docker-pull\" {
      image = \"$REDIS_IMAGE_NAME\"
      tag = \"$REDIS_IMAGE_TAG\"
    }
  }

  deploy {
    use \"docker\" {
      service_port=6379
    }
  }
}

app \"postgres\" {
  config {
    env = {
      POSTGRES_USER = \"$POSTGRES_USER\"
      POSTGRES_PASSWORD = \"$POSTGRES_PASSWORD\"
      POSTGRES_DB = \"box-db\"
    }
  }

  build {
    use \"docker-pull\" {
      image = \"$POSTGRES_IMAGE_NAME\"
      tag = \"$POSTGRES_IMAGE_TAG\"
    }
  }

  deploy {
    use \"docker\" {
      service_port=5432
    }
  }
}

app \"backend\" {
  config {
    env = {
      POSTGRES_USER = \"$POSTGRES_USER\"
      POSTGRES_PASSWORD = \"$POSTGRES_PASSWORD\"
      POSTGRES_HOST = \"POSTGRES_CONTAINER_NAME\"
      POSTGRES_DB = \"box-db\"
      REDIS_HOST = \"REDIS_CONTAINER_NAME\"
      REDIS_PORT = 6379
      JWT_SECRET = \"$JWT_SECRET\"
    }
  }

  build {
    use \"docker-pull\" {
      image = \"$BACKEND_IMAGE_NAME\"
      tag = \"$BACKEND_IMAGE_TAG\"
    }
  }

  deploy {
    use \"docker\" {
      service_port=8000
    }
  }
}

app \"backend-worker\" {
  config {
    env = {
      POSTGRES_USER = \"$POSTGRES_USER\"
      POSTGRES_PASSWORD = \"$POSTGRES_PASSWORD\"
      POSTGRES_HOST = \"POSTGRES_CONTAINER_NAME\"
      POSTGRES_DB = \"box-db\"
      REDIS_HOST = \"REDIS_CONTAINER_NAME\"
      REDIS_PORT = 6379
      JWT_SECRET = \"$JWT_SECRET\"
    }
  }

  build {
    use \"docker-pull\" {
      image = \"$BACKEND_IMAGE_NAME\"
      tag = \"$BACKEND_IMAGE_TAG\"
    }
  }

  deploy {
    use \"exec\" {
      command = [
        \"docker\", \"run\", \"-d\", \"--network\", \"waypoint\", \"-e\", \"JWT_SECRET=$JWT_SECRET\", \"-e\", \"POSTGRES_USER=$POSTGRES_USER\", \"-e\", \"POSTGRES_PASSWORD=$POSTGRES_PASSWORD\", \"-e\", \"POSTGRES_HOST=POSTGRES_CONTAINER_NAME\", \"-e\", \"POSTGRES_DB=box-db\", \"-e\", \"REDIS_HOST=REDIS_CONTAINER_NAME\", \"-e\", \"REDIS_PORT=6379\", \"$BACKEND_IMAGE_NAME:$BACKEND_IMAGE_TAG\", \"celery\", \"worker\", \"--app=app.worker.celery\", \"--concurrency=20\", \"--loglevel=INFO\"
      ]
    }
  }
}

app \"frontend\" {
  build {
    use \"docker-pull\" {
      image = \"$FRONTEND_IMAGE_NAME\"
      tag = \"$FRONTEND_IMAGE_TAG\"
    }
  }

  deploy {
    use \"docker\" {
      service_port=80
    }
  }
}
